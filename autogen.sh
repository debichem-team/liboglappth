#!/bin/sh

echo "Running libtoolize"
libtoolize --force --copy

echo "Removing autom4te.cache"
rm -fr autom4te.cache
rm -f aclocal.m4

##OSTYPE=`uname -s`
##AMFLAGS="--add-missing"
##if test "$OSTYPE" = "IRIX" -o "$OSTYPE" = "IRIX64"; then
##   AMFLAGS=$AMFLAGS" --include-deps";
##fi
##echo "Running aclocal"
##aclocal --force
##echo "Running automake"
##automake $AMFLAGS
##echo "Running autoconf"
##autoconf

echo "Running autoreconf"

##export AUTOCONF=autoconf-2.13
##export AUTOHEADER=autoheader-2.13

##export ACLOCAL=aclocal-1.4
##export AUTOMAKE=automake-1.4

autoreconf -v -f

echo "======================================"
echo "Now you are ready to run './configure'"
echo "======================================"

