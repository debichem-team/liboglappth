liboglappth (1.0.0-3) unstable; urgency=low

  * Team upload

  [ Daniel Leidert (dale) ]
  * debian/control (Uploaders): Removed myself.

  [ Andreas Tille ]
  * Rebuilding to fix ghemical display
    Closes: #826069
  * d/rules: remove default options for higher compat level
  * Drop (Build-)Depends on obsolete package libgl1-mesa-dev and
    libglu1-mesa-dev
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * d/watch:
     - version=4
     - seccure URI
  * Enable building twice in a row
    Closes: #1047996
  * Set hardening options

 -- Andreas Tille <tille@debian.org>  Fri, 09 Aug 2024 21:43:01 +0200

liboglappth (1.0.0-2) unstable; urgency=low

  * debian/control (Standards-Version): Bumped to 3.9.3.
  * debian/liboglappth2.symbols: Removed (closes: #667252).

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Wed, 02 May 2012 23:00:27 +0200

liboglappth (1.0.0-1) unstable; urgency=low

  * New upstream release.
  * debian/compat: Increased dh compat level.
  * debian/control (Standards-Version): Bumped to 3.9.2.
    (Build-Depends): Increased debhelper version.
    (Vcs-Svn): Fixed vcs-field-uses-not-recommended-uri-format.
    (Vcs-Browser): Set to correct URL.
  * debian/copyright: Minor update.
  * debian/liboglappth2.symbols: Added symbols file.
  * debian/rules: Rewritten for dh 7.
  * debian/source/format: Added and set to 3.0 (quilt).

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 27 Nov 2011 17:34:27 +0100

liboglappth (0.98-2) unstable; urgency=low

  * Rebuild for unstable.

  * debian/control (Vcs-Svn): Set back to unstable location.
    (Description): Fixed extended-description-is-probably-too-short and
    duplicate-long-description lintian hints.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 22 Feb 2009 00:01:27 +0100

liboglappth (0.98-1) experimental; urgency=low

  * New upstream release 0.98.

  * debian/control: liboglappth1 became liboglappth2.
    (Uploaders): Removed LI. Thanks for your work!
    (Build-Depends): Removed freeglut3-dev and dpatch.
    (Standards-Version): Bumped to 3.8.0.
    (Vcs-Svn): Set to experimental location.
    (Depends): Added ${misc:Depends}. Replaced liboglappth1 with liboglappth2.
    (Description): Improved short description.
  * debian/copyright: Improved a bit.
  * debian/liboglappth1.install: Renamed to debian/liboglappth2.install.
  * debian/rules: Removed some sample boilerplates. Added CXXFLAGS. Removed
    dpatch environment.
    (LDFLAGS): Link with -lGL -lGLU.
  * debian/patches/01_liboglappth_pc_in.dpatch: Removed (applied upstream).
  * debian/patches/02_gcc43.dpatch: Ditto.
  * debian/patches/: Removed dpatch environnment.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sat, 31 Jan 2009 16:24:45 +0100

liboglappth (0.96-4) unstable; urgency=low

  [ LI Daobing ]
  * FTBFS with GCC 4.3: missing #includes (Closes: #462071)

  [ Daniel Leidert ]
  * debian/control: Added DM-Upload-Allowed for DM status.
    (Uploaders): Added myself.
  * debian/rules: Implemented noopt build option by defining CFLAGS. Further
    made some minor cleanup.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Mon, 10 Mar 2008 01:30:16 +0100

liboglappth (0.96-3) unstable; urgency=low

  * debian/copyright: Point to GPL-2, not GPL.

 -- Michael Banck <mbanck@debian.org>  Sat, 05 Jan 2008 18:29:05 +0100

liboglappth (0.96-2) unstable; urgency=low

  * debian/copyright: Clarify that liboglappth is GPLv2only.
  * debian/control (Uploaders): Added myself.

 -- Michael Banck <mbanck@debian.org>  Sun, 30 Dec 2007 01:52:22 +0100

liboglappth (0.96-1) unstable; urgency=low

  * Initial release (Closes: #447745).

 -- LI Daobing <lidaobing@gmail.com>  Tue, 23 Oct 2007 21:56:49 +0800

