// BASE_APP.CPP

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "base_app.h"

#include <stdlib.h>	// the definition for NULL...

#include <algorithm>	// for WIN32...
using namespace std;

// NOT_DEFINED is defined here the same way as in libghemical.

#ifndef NOT_DEFINED
#define NOT_DEFINED -1
#endif	// NOT_DEFINED

/*################################################################################################*/

mouseinfo::mi_state mouseinfo::state = mouseinfo::sUnknown;
mouseinfo::mi_button mouseinfo::button = mouseinfo::bNone;

bool mouseinfo::shift_down = false;
bool mouseinfo::ctrl_down = false;

float mouseinfo::ang_sensitivity = 180.0;
float mouseinfo::dist_sensitivity = 2.0;

int mouseinfo::latest_x = -1;
int mouseinfo::latest_y = -1;

/*################################################################################################*/

base_app * base_app::app = NULL;

base_app::base_app(void)
{
	if (app != NULL)	// prevent multiple instances...
	{
		app->ErrorMessage("liboglappth : base_app ctor failed!");
		exit(EXIT_FAILURE);
	}
	
	app = this;
	
	glname_counter = 1;	// reserve value 0 for UNREGISTERED.
}

base_app::~base_app(void)
{
}

base_app * base_app::GetAppB(void)
{
	return app;
}

void base_app::AddCamera(ogl_camera * cam)
{
	vector<ogl_camera *>::iterator it = find(camera_vector.begin(), camera_vector.end(), cam);
	if (it != camera_vector.end())
	{
		cout << "liboglappth : duplicate cam record!" << endl;
		exit(EXIT_FAILURE);
	}
	
	camera_vector.push_back(cam);
}

bool base_app::RemoveCamera(ogl_camera * cam)
{
	vector<ogl_camera *>::iterator it = find(camera_vector.begin(), camera_vector.end(), cam);
	if (it == camera_vector.end()) return false;
	
	// make sure that the related lights are removed as well...
	
	int n1 = 0;
	while (n1 < (int) light_vector.size())
	{
		if (light_vector[n1]->owner == cam)
		{
			RemoveLight(light_vector[n1]);
		}
		else n1++;
	}
	
// do not delete the object, just remove it from the vector...
// do not delete the object, just remove it from the vector...
// do not delete the object, just remove it from the vector...
	
	camera_vector.erase(it);
	return true;
}

bool base_app::AddGlobalLight(ogl_light * light)
{
	GLint max_local_size = 0;
	for (unsigned int n1 = 0;n1 < camera_vector.size();n1++)
	{
		GLint local_size = CountLocalLights(camera_vector[n1]);
		if (local_size > max_local_size) max_local_size = local_size;
	}
	
	GLint total_lights = CountGlobalLights() + max_local_size;
	
// we really can't do this test here ; see SetupLights()...
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//GLint max_lights; glGetIntegerv(GL_MAX_LIGHTS, & max_lights);
//if (total_lights == max_lights) return false;
	
	light_vector.push_back(light);
	SetGlobalLightNumbers();
	
	for (unsigned int n1 = 0;n1 < camera_vector.size();n1++)
	{
		SetLocalLightNumbers(camera_vector[n1]);
		SetupLights(camera_vector[n1]);
	}
	
	return true;
}

bool base_app::AddLocalLight(ogl_light * light, ogl_camera * cam)
{
	GLint total_lights = CountGlobalLights() + CountLocalLights(cam);
	
// we really can't do this test here ; see SetupLights()...
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//GLint max_lights; glGetIntegerv(GL_MAX_LIGHTS, & max_lights);
//if (total_lights == max_lights) return false;
	
	light->owner = cam; light_vector.push_back(light);
	SetLocalLightNumbers(cam);
	SetupLights(cam);
	
	return true;
}

bool base_app::RemoveLight(ogl_dummy_object * obj)
{
	int n1 = IsLight(obj);
	if (n1 < 0) return false;
	
	ogl_camera * owner = light_vector[n1]->owner;
	
// do not delete the object, just remove it from the vector...
// do not delete the object, just remove it from the vector...
// do not delete the object, just remove it from the vector...
	
	light_vector.erase(light_vector.begin() + n1);
	
	if (owner != NULL)
	{
		SetLocalLightNumbers(owner);
		SetupLights(owner);
	}
	else
	{
		SetGlobalLightNumbers();
		for (unsigned int n1 = 0;n1 < camera_vector.size();n1++)
		{
			SetLocalLightNumbers(camera_vector[n1]);
			SetupLights(camera_vector[n1]);
		}
	}
	
	return true;
}

int base_app::IsLight(const ogl_dummy_object * obj)
{
	int index = NOT_DEFINED;
	for (unsigned int n1 = 0;n1 < light_vector.size();n1++)
	{
		if (light_vector[n1] == obj) index = (int) n1;
	}
	
	return index;
}

GLint base_app::CountGlobalLights(void)
{
	GLint sum = 0;
	unsigned int n1 = 0;
	while (n1 < light_vector.size())
	{
		if (light_vector[n1++]->owner == NULL) sum++;
	}
	
	return sum;
}

GLint base_app::CountLocalLights(ogl_camera * cam)
{
	GLint sum = 0;
	unsigned int n1 = 0;
	while (n1 < light_vector.size())
	{
		if (light_vector[n1++]->owner == cam) sum++;
	}
	
	return sum;
}

void base_app::SetupLights(ogl_camera * cam)
{
	for (unsigned int n1 = 0;n1 < cam->wnd_vector.size();n1++)
	{
		if (!cam->wnd_vector[n1]->SetCurrent())
		{
			cout << "liboglappth : GL is not yet initialized -> skipping light setup!" << endl;
			continue;
		}
		
		GLint max_lights;
		glGetIntegerv(GL_MAX_LIGHTS, & max_lights);
		
		for (GLint n2 = 0;n2 < max_lights;n2++)
		{
			glDisable((GLenum) (GL_LIGHT0 + n2));
		}
		
		for (unsigned int n2 = 0;n2 < light_vector.size();n2++)
		{
			bool test1 = (light_vector[n2]->owner != NULL);
			bool test2 = (light_vector[n2]->owner != cam);
			if (test1 && test2) continue;
			
			light_vector[n2]->SetupProperties(); bool test = false;
			if (light_vector[n2]->owner == NULL && cam->use_global_lights) test = true;
			if (light_vector[n2]->owner == cam && cam->use_local_lights) test = true;
			if (test) glEnable((GLenum) light_vector[n2]->number);
		}
	}
}

void base_app::SetGlobalLightNumbers(void)
{
	GLint counter = 0;
	for (unsigned int n1 = 0;n1 < light_vector.size();n1++)
	{
		if (light_vector[n1]->owner != NULL) continue;
		light_vector[n1]->number = GL_LIGHT0 + counter++;
	}
}

void base_app::SetLocalLightNumbers(ogl_camera * cam)
{
	GLint counter = CountGlobalLights();
	for (unsigned int n1 = 0;n1 < light_vector.size();n1++)
	{
		if (light_vector[n1]->owner != cam) continue;
		light_vector[n1]->number = GL_LIGHT0 + counter++;
	}
}

void base_app::UpdateLocalLightLocations(ogl_camera * cam)
{
	for (unsigned int n1 = 0;n1 < light_vector.size();n1++)
	{
		ogl_camera * owner = light_vector[n1]->owner;
		if (owner != NULL && owner != cam) continue;
		
		light_vector[n1]->SetupLocation();
	}
}

void base_app::RenderLights(ogl_camera * cam)
{
	for (unsigned int n1 = 0;n1 < light_vector.size();n1++)
	{
		ogl_camera * owner = light_vector[n1]->owner;
		if (owner != NULL && owner != cam) continue;
		
		light_vector[n1]->Render();
	}
}

//////////////////////////////////////////////////
//////////////////////////////////////////////////

GLuint base_app::RegisterGLName(void * ptr)
{
	GLuint newkey = glname_counter++;
	
	pair<GLuint, void *> p(newkey, ptr);
	glname_map.insert(p);
	
	return newkey;
}

void base_app::UnregisterGLNameByName(GLuint key)
{
	map<GLuint, void *>::iterator it = glname_map.find(key);
	if (it != glname_map.end())
	{
		glname_map.erase(it);
		return;
	}
	
	ErrorMessage("liboglappth : UnregisterGLNameByName() failed!");
}

void base_app::UnregisterGLNameByPtr(void * ptr)
{
	// warning!!! this is the slower way to unregister.
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	map<GLuint, void *>::iterator it = glname_map.begin();
	while (it != glname_map.end())
	{
		if ((* it).second == ptr)
		{
			glname_map.erase(it);
			return;
		}
		else it++;
	}
	
	ErrorMessage("liboglappth : UnregisterGLNameByPtr() failed!");
}

void * base_app::FindPtrByGLName(GLuint key)
{
	map<GLuint, void *>::iterator it = glname_map.find(key);
	if (it != glname_map.end()) return (* it).second;
	else
	{
		ErrorMessage("liboglappth : FindPtrByGLName() failed!");;
		return NULL;
	}
}

bool base_app::AddTP(void * owner, transparent_primitive & tp)
{
	if (!tp.TestOwner(owner)) return false;		// this is just a double check, to make
	tp_vector.push_back(tp); return true;		// sure that we have correct "owner"...
}

void base_app::UpdateMPsForAllTPs(void * owner)
{
	for (unsigned int n1 = 0;n1 < tp_vector.size();n1++)
	{
		if (tp_vector[n1].TestOwner(owner)) tp_vector[n1].GetData()->UpdateMP();
	}
}

void base_app::RenderAllTPs(ogl_camera * cam)
{
	// here we will render those transparent primitives...
	// all views should call this last when rendering the contents!
	
	// first we must update the distances for all TP's, and sort them...
	
	const GLfloat * ref1 = cam->GetSafeLD()->crd;
	const GLfloat * ref2 = cam->GetSafeLD()->zdir.data;
	
	for (unsigned int n1 = 0;n1 < tp_vector.size();n1++)
	{
		tp_vector[n1].UpdateDistance(ref1, ref2);
	}
	
	sort(tp_vector.begin(), tp_vector.end());
	
	// and then we will just render them...
	// it looks better if we disable depth buffer changes...
	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(false); glEnable(GL_BLEND);
	
	for (unsigned int n1 = 0;n1 < tp_vector.size();n1++)
	{
		tp_vector[n1].GetData()->Render();
	}
	
	glDisable(GL_BLEND); glDepthMask(true);
}

void base_app::RemoveAllTPs(void * owner)
{
	unsigned int n1 = 0;
	while (n1 < tp_vector.size())
	{
		vector<transparent_primitive>::iterator iter;
		
		if (!tp_vector[n1].TestOwner(owner))
		{
			n1++;
		}
		else
		{
			delete tp_vector[n1].GetData();		// destroy the data object!!!
			
			iter = (tp_vector.begin() + n1);
			tp_vector.erase(iter);
		}
	}
}

/*################################################################################################*/

// eof
