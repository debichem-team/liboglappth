// BASE_APP.H : an OpenGL application base class.

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

//#include "config.h"

#ifndef BASE_APP_H
#define BASE_APP_H

/*################################################################################################*/

#ifdef WIN32
#include <windows.h>	// need to have this before the GL stuff...
#endif	// WIN32

#include <GL/gl.h>
#include <GL/glu.h>

#include <map>
#include <vector>
using namespace std;

/*################################################################################################*/

struct mouseinfo;

class base_app;

#include "base_wnd.h"

#include "ogl_camera.h"
#include "ogl_lights.h"

#include "transparent.h"

/*################################################################################################*/

struct mouseinfo
{
	enum mi_state { sUp, sDown, sUnknown };
	enum mi_button { bLeft, bMiddle, bRight, bNone };
	
	static mi_state state;
	static mi_button button;
	
	static bool shift_down;
	static bool ctrl_down;
	
	static float ang_sensitivity;
	static float dist_sensitivity;
	
	static int latest_x;
	static int latest_y;
};

/*################################################################################################*/

class base_app
{
	private:
	
	static base_app * app;
	
	protected:
	
	vector<ogl_camera *> camera_vector;
	vector<ogl_light *> light_vector;
	
	GLuint glname_counter;			// these are for generating GLuint-type
	map<GLuint, void *> glname_map;		// names for objects (or memory blocks).
	
	vector<transparent_primitive> tp_vector;
	
// friends...
// ^^^^^^^^^^
	friend class ogl_camera;
	
	public:
	
	base_app(void);
	virtual ~base_app(void);
	
	static base_app * GetAppB(void);
	
	// simple messaging methods:
	// ^^^^^^^^^^^^^^^^^^^^^^^^^
	
	virtual void Message(const char *) = 0;
	virtual void WarningMessage(const char *) = 0;
	virtual void ErrorMessage(const char *) = 0;
	virtual bool Question(const char *) = 0;
	
	// some camera/light-related methods:
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	virtual void AddCamera(ogl_camera *);
	virtual bool RemoveCamera(ogl_camera *);
	
	virtual bool AddGlobalLight(ogl_light *);
	virtual bool AddLocalLight(ogl_light *, ogl_camera *);
	virtual bool RemoveLight(ogl_dummy_object *);
	
	int IsLight(const ogl_dummy_object *);
	
	GLint CountGlobalLights(void);
	GLint CountLocalLights(ogl_camera *);
	
	void SetupLights(ogl_camera *);
	
	void SetGlobalLightNumbers(void);
	void SetLocalLightNumbers(ogl_camera *);
	
	void UpdateLocalLightLocations(ogl_camera *);
	void RenderLights(ogl_camera *);
	
	// OpenGL selection naming methods:
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	GLuint RegisterGLName(void *);
	void UnregisterGLNameByName(GLuint);
	void UnregisterGLNameByPtr(void *);
	void * FindPtrByGLName(GLuint);
	
	// transparent primitive rendering methods:
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
	bool AddTP(void *, transparent_primitive &);
	void UpdateMPsForAllTPs(void *);
	void RenderAllTPs(ogl_camera *);
	void RemoveAllTPs(void *);
};

/*################################################################################################*/

#endif	// BASE_APP_H

// eof
