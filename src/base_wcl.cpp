// BASE_WCL.CPP

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "base_wcl.h"

#include "base_app.h"
#include <stdlib.h>	// the definition for NULL...

#include <GL/gl.h>
#include <GL/glu.h>

#include <cstring>
using namespace std;

/*################################################################################################*/

base_wcl::base_wcl(ogl_camera * c)
{
	wnd = NULL;
	
	cam = c;
	delete_cam_plz = false;
	cam->RegisterClient(this);
	
	if (!cam->update_vdim)
	{
		vdim[0] = 1.0;	// just set some default values here...
		vdim[1] = 1.0;	// just set some default values here...
	}
	
	title = NULL;
}

base_wcl::~base_wcl(void)
{
	if (wnd != NULL)
	{
		base_app * app = base_app::GetAppB();
		app->ErrorMessage("liboglappth : base_wcl::wnd memleak!");
	}
	
	cam->UnregisterClient(this);
	
	if (delete_cam_plz)
	{
		delete cam;
		cam = NULL;
	}
	
	if (title != NULL)
	{
		delete[] title;
		title = NULL;
	}
}

base_wnd * base_wcl::GetWnd(void)
{
	return wnd;
}

ogl_camera * base_wcl::GetCam(void)
{
	return cam;
}

void base_wcl::LinkWnd(base_wnd * w)
{
	if (wnd != NULL)
	{
		base_app * app = base_app::GetAppB();
		app->ErrorMessage("liboglappth : base_wcl::LinkWnd() : wnd already in use!");
	}
	
	if (w->wcl != NULL)
	{
		base_app * app = base_app::GetAppB();
		app->ErrorMessage("liboglappth : base_wcl::LinkWnd() : link overwrite!");
	}
	
	w->wcl = this;
	wnd = w;
	
	cam->RegisterWnd(wnd);
	
	// if the window is realized (so that SetCurrent() may be called)
	// but is not initialized yet, then do the initialization...
	
	if (w->is_realized && !w->is_initialized)
	{
		w->is_initialized = true;
		
		w->SetCurrent();
		w->GetClient()->InitGL();
		w->RequestUpdate(false);
	}
	
	// if we have a title available, then ask the window to show it...
	
	if (title != NULL) w->TitleChanged();
}

void base_wcl::UnlinkWnd(void)
{
	if (!wnd)
	{
		base_app * app = base_app::GetAppB();
		app->ErrorMessage("liboglappth : base_wcl::UnlinkWnd() : wnd is already NULL!");
	}
	
	if (wnd->wcl == NULL)
	{
		base_app * app = base_app::GetAppB();
		app->ErrorMessage("liboglappth : base_wcl::UnlinkWnd() : wcl is already NULL!");
	}
	
	cam->UnregisterWnd(wnd);
	
	wnd->wcl = NULL;
	wnd = NULL;
}

const char * base_wcl::GetTitle(void)
{
	return (const char *) title;
}

void base_wcl::SetTitle(const char * t)
{
	if (t == NULL)
	{
		cout << "liboglappth : ERROR : base_wcl::SetTitle() got NULL." << endl;
		exit(EXIT_FAILURE);
	}
	
	if (title != NULL)
	{
		delete[] title;
		title = NULL;
	}
	
	title = new char[strlen(t) + 1];
	strcpy(title, t);
	
	if (wnd != NULL) wnd->TitleChanged();
}

/*################################################################################################*/

// eof
