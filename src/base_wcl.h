// BASE_WCL.H : a platform-independent window client base class.

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

//#include "config.h"

#ifndef BASE_WCL_H
#define BASE_WCL_H

/*################################################################################################*/

class base_wcl;

#include "base_wnd.h"
#include "ogl_camera.h"

/*################################################################################################*/

// base_wcl (window client) class defines the properties of all window
// client classes.

// a window client object knows how to display information in a window
// (using OpenGL API commands) and how to process the user input which
// the user generates using mouse events.

class base_wcl
{
	private:
	
	base_wnd * wnd;		// link/unlink will handle this...
	
	protected:
	
// cam is set protected so that a derived object can set it's own camera
// object at ctor and also delete it at dtor ; no other access is needed.
	
	ogl_camera * cam;
	bool delete_cam_plz;	// true if derived class cannot delete...
	
	float vdim[2];		// view dimensions, in dist-units.
	char * title;
	
	friend class ogl_camera;
	
	public:
	
	base_wcl(ogl_camera *);
	virtual ~base_wcl(void);
	
	base_wnd * GetWnd(void);
	ogl_camera * GetCam(void);
	
	void LinkWnd(base_wnd *);
	void UnlinkWnd(void);
	
	const char * GetTitle(void);
	void SetTitle(const char *);
	
	virtual void ButtonEvent(int, int) = 0;
	virtual void MotionEvent(int, int) = 0;
	
	virtual void UpdateWnd(void) = 0;
	
	virtual void InitGL(void) = 0;
	virtual void RenderGL(rmode) = 0;
};

/*################################################################################################*/

#endif	// BASE_WCL_H

// eof
