// BASE_WND.CPP

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "base_wnd.h"

#include "base_app.h"
#include <stdlib.h>	// the definition for NULL...

/*################################################################################################*/

base_wnd::base_wnd()
{
	wcl = NULL;
	
	is_realized = false;
	is_initialized = false;
	
	size[0] = size[1] = -1;
}

base_wnd::~base_wnd(void)
{
	if (wcl != NULL)
	{
		base_app * app = base_app::GetAppB();
		app->ErrorMessage("base_wnd dtor : unlinked!");
	}
}

base_wcl * base_wnd::GetClient(void)
{
	return wcl;
}

bool base_wnd::GetRealized(void)
{
	return is_realized;
}

bool base_wnd::GetInitialized(void)
{
	return is_initialized;
}

int base_wnd::GetWidth(void)
{
	return size[0];
}

int base_wnd::GetHeight(void)
{
	return size[1];
}

bool base_wnd::IsTimerON(void)
{
	cout << "liboglappth : base_wnd::IsTimerON() called." << endl;
	return false;
}

void base_wnd::SetTimerON(int msec)
{
	cout << "liboglappth : base_wnd::SetTimerON() called, msec = " << msec << "." << endl;
}

void base_wnd::SetTimerOFF(void)
{
	cout << "liboglappth : base_wnd::SetTimerOFF() called." << endl;
}

void base_wnd::SetRealized(void)
{
	is_realized = true;
}

void base_wnd::SetInitialized(void)
{
	is_initialized = true;
}

void base_wnd::SetWidth(int w)
{
	size[0] = w;
}

void base_wnd::SetHeight(int h)
{
	size[1] = h;
}

/*################################################################################################*/

// eof
