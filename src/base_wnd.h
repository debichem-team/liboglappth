// BASE_WND.H : an OpenGL window base class.

// Copyright (C) 2005 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

//#include "config.h"

#ifndef BASE_WND_H
#define BASE_WND_H

/*################################################################################################*/

class base_wnd;

#include "base_wcl.h"
#include "ogl_camera.h"

/*################################################################################################*/

// base_wnd class defines the properties common to all wnd (window) classes.

// a window class is always an OpenGL window. it should implement a pop-up
// menu which is always operated using the right mouse button.

// a window is (normally) linked to a window client object, which knows how
// to display the information and how to process the user inputs (mouse
// movements and clicks other than popup-menu-operation related ones).

class base_wnd
{
	private:
	
	base_wcl * wcl;		// link/unlink will handle this...
	
	bool is_realized;	// true if SetCurrent() etc works...
	bool is_initialized;	// true if InitGL() is called...
	
	int size[2];		// size of the window, in pixels.
	
	friend class base_wcl;
	
	public:
	
	base_wnd(void);
	virtual ~base_wnd(void);
	
	base_wcl * GetClient(void);
	
	bool GetRealized(void);
	bool GetInitialized(void);
	
	int GetWidth(void);
	int GetHeight(void);
	
	virtual void RequestUpdate(bool) = 0;
	virtual void RequestResize(int, int) = 0;
	
	virtual bool SetCurrent(void) = 0;
	
	virtual bool IsTimerON(void);
	
	virtual void SetTimerON(int);
	virtual void SetTimerOFF(void);
	
	protected:
	
	void SetRealized(void);
	void SetInitialized(void);
	
	void SetWidth(int);
	void SetHeight(int);
	
	virtual void TitleChanged(void) = 0;
};

/*################################################################################################*/

#endif	// BASE_WND_H

// eof
