// OGL_CAMERA.CPP

// Copyright (C) 1998 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ogl_camera.h"		// config.h is here -> we get ENABLE-macros here...

#include "base_app.h"

#include <vector>
#include <algorithm>	// for WIN32...
#include <sstream>
using namespace std;

// NOT_DEFINED is defined here the same way as in libghemical.

#ifndef NOT_DEFINED
#define NOT_DEFINED -1
#endif	// NOT_DEFINED

/*################################################################################################*/

ogl_camera::ogl_camera(const ogl_object_location & p1, GLfloat p2) :
	ogl_dummy_object(p1)
{
	focus = p2;
	clipping = 0.90;
	
	update_vdim = true;
	
	use_local_lights = true;
	use_global_lights = true;
	
	ortho = false;
	
	stereo_mode = false;
	stereo_relaxed = false;
	
	stereo_displacement = -0.25;
	relaxed_separation = 0.60;
	
	GetLD()->crd[2] = -focus;
}

// this is partial because only ogl_camera::OrbitObject() needs this!!!
// this is partial because only ogl_camera::OrbitObject() needs this!!!
// this is partial because only ogl_camera::OrbitObject() needs this!!!

ogl_camera::ogl_camera(const ogl_camera & p1) :
	ogl_dummy_object(* p1.ol)
{
}

ogl_camera::~ogl_camera(void)
{
	if (!obj_list.empty())
	{
		cout << "liboglappth : warning!!! ogl_camera::obj_list not empty!" << endl;
	}
	
	if (!wnd_vector.empty())
	{
		cout << "liboglappth : error!!! ogl_camera::wnd_vector not empty!" << endl;
		exit(EXIT_FAILURE);
	}
}

void ogl_camera::RegisterClient(base_wcl * wcl)
{
	vector<base_wcl *>::iterator it = find(wcl_vector.begin(), wcl_vector.end(), wcl);
	if (it != wcl_vector.end())
	{
		cout << "liboglappth : duplicate wcl record!" << endl;
		exit(EXIT_FAILURE);
	}
	
	wcl_vector.push_back(wcl);
}

void ogl_camera::UnregisterClient(base_wcl * wcl)
{
	vector<base_wcl *>::iterator it = find(wcl_vector.begin(), wcl_vector.end(), wcl);
	if (it == wcl_vector.end())
	{
		cout << "liboglappth : wcl record not found!" << endl;
		exit(EXIT_FAILURE);
	}
	
	wcl_vector.erase(it);
}

void ogl_camera::RegisterWnd(base_wnd * wnd)
{
	vector<base_wnd *>::iterator it = find(wnd_vector.begin(), wnd_vector.end(), wnd);
	if (it != wnd_vector.end())
	{
		cout << "liboglappth : duplicate wnd record!" << endl;
		exit(EXIT_FAILURE);
	}
	
	wnd_vector.push_back(wnd);
}

void ogl_camera::UnregisterWnd(base_wnd * wnd)
{
	vector<base_wnd *>::iterator it = find(wnd_vector.begin(), wnd_vector.end(), wnd);
	if (it == wnd_vector.end())
	{
		cout << "liboglappth : wnd record not found!" << endl;
		exit(EXIT_FAILURE);
	}
	
	wnd_vector.erase(it);
}

bool ogl_camera::CopySettings(const ogl_camera * p1)
{
	ogl_ol_static * ref = dynamic_cast<ogl_ol_static *>(ol);
	if (ref == NULL) return false;
	
	// now that we have verified that we can modify the ol-object, we will copy the settings...
	
	focus = p1->focus; clipping = p1->clipping;
	for (int n1 = 0;n1 < 3;n1++) GetLD()->crd[n1] = p1->GetSafeLD()->crd[n1];
	GetLD()->zdir = p1->GetSafeLD()->zdir;
	GetLD()->ydir = p1->GetSafeLD()->ydir;
	
	// the local lights that are attached to the camera are not moved?!?!?!
	// the local lights that are attached to the camera are not moved?!?!?!
	// the local lights that are attached to the camera are not moved?!?!?!
	
	return true;
}

void ogl_camera::RenderScene(base_wnd * wnd, bool accum, bool pick, int x, int y)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	// projection matrix is now initialized. if this is a "pick" operation, we must do
	// some furter manipulations (gluPickMatrix() is used to "zoom" into the selection area).
	
	if (pick)
	{
		glRenderMode(GL_SELECT);
		GLint vp[4]; glGetIntegerv(GL_VIEWPORT, vp);
		
		// is this dependent on screen resolution or what???
		// there are some problems with selection in wireframe models...
		
		const GLfloat region = 5.0;
		gluPickMatrix(x, vp[3] - y, region, region, vp);
	}
	
	if (accum) glClear(GL_ACCUM_BUFFER_BIT);
	else glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	// the stereo-modes related stuff start here...
	// the stereo-modes related stuff start here...
	// the stereo-modes related stuff start here...
	
	// the stereo modes are implemented as a loop, that is cycled either once or twice.
	
	int width = wnd->GetWidth(); if (stereo_mode && stereo_relaxed) width /= 2;
	int height = wnd->GetHeight(); int shift = 0;
	
	const GLfloat aspect = (GLfloat) width / (GLfloat) height;
	const GLfloat fovy = (aspect > 1.0 ? 45.0 / aspect : 45.0);
	
	if (update_vdim)
	{
		wnd->GetClient()->vdim[1] = focus * tan(M_PI * fovy / 360.0);
		wnd->GetClient()->vdim[0] = aspect * wnd->GetClient()->vdim[1];
	}
	
	const int stereo_count = (stereo_mode ? 2 : 1);
	for (int stereo_loop = 0;stereo_loop < stereo_count;stereo_loop++)
	{
		glViewport(shift, 0, width, height);
		if (stereo_mode && stereo_relaxed)
		{
			// this will shift our viewport right at the next round...
			
			shift += width;
		}
		
		const GLfloat nearplane = (1.0 - clipping) * focus;
		const GLfloat farplane = (1.0 + clipping) * focus;
		
		if (!ortho)
		{
			gluPerspective(fovy, aspect, nearplane, farplane);
		}
		else
		{
			const GLfloat vdimX = wnd->GetClient()->vdim[0];
			const GLfloat vdimY = wnd->GetClient()->vdim[1];
			
			glOrtho(-vdimX, +vdimX, -vdimY, +vdimY, nearplane, farplane);
		}
		
		const ogl_obj_loc_data * loc1 = GetSafeLD();
		
		glMatrixMode(GL_MODELVIEW); glLoadIdentity();
		oglv3d<GLfloat> target = oglv3d<GLfloat>(loc1->crd) + (loc1->zdir * focus);
		
		const GLfloat * r1; const GLfloat * r2; const GLfloat * r3;
		
		if (!stereo_mode)
		{
			// if not a stereo mode, then just use normal camera settings...
			
			r1 = loc1->crd;		// eye coordinates
			r2 = target.data;	// target coordinates
			r3 = loc1->ydir.data;	// y-direction vector
		}
		else
		{
			// for all stereo modes, translate the camera along camera's x-axis.
			// y-direction will remain constant, but other directions won't. does it matter???
			
			GLfloat displacement = stereo_displacement / 20.0;
			if (!stereo_loop) displacement = -displacement;
			
			oglv3d<GLfloat> xdir = loc1->ydir.vpr(loc1->zdir);
			
	// displace each eye coordinate using camera's x-axis -> will be independent on camera's orientation!!!
	// displace each eye coordinate using camera's x-axis -> will be independent on camera's orientation!!!
	// displace each eye coordinate using camera's x-axis -> will be independent on camera's orientation!!!
			
			static GLfloat tmp_crd[3];
			tmp_crd[0] = loc1->crd[0] + displacement * xdir.data[0];
			tmp_crd[1] = loc1->crd[1] + displacement * xdir.data[1];
			tmp_crd[2] = loc1->crd[2] + displacement * xdir.data[2];
			
			r1 = tmp_crd;		// eye coordinates
			r2 = target.data;	// target coordinates
			r3 = loc1->ydir.data;	// y-direction vector
			
			if (!stereo_relaxed)
			{
				// for the red-blue stereo mode, set the color masks...
				
				if (!stereo_loop) glColorMask(GL_TRUE,GL_FALSE,GL_FALSE,GL_TRUE);	// left_eye - red
				else glColorMask(GL_FALSE,GL_FALSE,GL_TRUE,GL_TRUE);			// right_eye - blue
				
				// ...and clear the depth buffer!!!
				
				glClear(GL_DEPTH_BUFFER_BIT);
			}
		}
		
		gluLookAt(r1[0], r1[1], r1[2], r2[0], r2[1], r2[2], r3[0], r3[1], r3[2]);
		
		if (stereo_mode && stereo_relaxed)
		{
			// this will do the "relaxed-eye stereo" separation effect.
			
			// 20061005 ; this is not as good as it could be, since the
			// effect is not a pure displacement but also a small rotation
			// -> interferes with the stereo_displacement option!!!!!
			
			GLfloat separation = relaxed_separation / 10.0;
			if (!stereo_loop) separation = -separation;
			else separation *= 2.0;
			
			glMatrixMode(GL_PROJECTION);
			glTranslatef(separation, 0.0, 0.0);
			glMatrixMode(GL_MODELVIEW);
		}
		
		// now, it's time to set up the lights...
		
		base_app::GetAppB()->UpdateLocalLightLocations(this);
		
		// here, we are finally ready to actually render the view. for simple non-stereo views, just erase the background
		// by calling glClear(), and then call prj->Render() to render the view. For relaxed-eye stereo views, two viewports
		// are used and the view is therefore rendered twice, calling glClear only once. TODO(???): there is also a hardware
		// stereo mode (not much supported yet, except SGI) that could be also used; that is a special graphics mode with two
		// color buffers, and glDrawBuffer(???) is called to determine which is active.
		
		// to make things more complicated, there is the "accumulation" effect that relies on glClear() call. to make all this
		// work together, we must take all glClear() and glAccum() calls out from the prj->Render, and do them here.
		
	// this "accumulation" stuff (used only in gt2 so far) is not tested after stereo mode changes...
	// this "accumulation" stuff (used only in gt2 so far) is not tested after stereo mode changes...
	// this "accumulation" stuff (used only in gt2 so far) is not tested after stereo mode changes...
		
		glInitNames();
		if (ogl_transformer::transform_in_progress)
		{
			// do the "transformation" effect:
			// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			// 1) render all non-selected parts of the scene.
			// 2) render the selected parts translated/rotated as told by the transformer.
			
			// accumulation should be DISABLED here!!!
			// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			
			wnd->GetClient()->RenderGL(Transform1);
			
			glPushMatrix();
			ogl_transformer::client->tc_object_ref->SetModelView();
			wnd->GetClient()->RenderGL(Transform2);
			glPopMatrix();
		}
		else
		{
			// just do the normal rendering...
			
			wnd->GetClient()->RenderGL(Normal);
		}
		
		if (stereo_mode && !stereo_relaxed)
		{
			// reset the color mask...
			
			glColorMask(GL_TRUE,GL_TRUE,GL_TRUE,GL_TRUE);
		}
	}
	
	// end of stereo-modes related stuff...
	// end of stereo-modes related stuff...
	// end of stereo-modes related stuff...
}

// when rotating or translating a camera we must make all operations in reversed directions
// and also drag all those local lights along...

void ogl_camera::OrbitObject(const GLfloat * ang, const ogl_camera & cam)
{
	GLfloat tmp_ang[3];
	for (int n1 = 0;n1 < 3;n1++)
	{
		tmp_ang[n1] = -ang[n1];
	}
	
	base_app * app = base_app::GetAppB();
	for (unsigned int n1 = 0;n1 < app->light_vector.size();n1++)
	{
		if (app->light_vector[n1]->owner != this) continue;
		app->light_vector[n1]->OrbitObject(tmp_ang, cam);
	}
	
	ogl_dummy_object::OrbitObject(tmp_ang, cam);
	
	DoCameraEvents();
}

// when rotating a camera, we must also make the local lights to orbit around the camera.
// dummy_object::OrbitObject() will orbit the lights around the camera's focus point, so here
// we must set the camera focus to zero before making the transformation...

void ogl_camera::RotateObject(const GLfloat * ang, const ogl_camera & cam)
{
	GLfloat tmp_ang[3];
	for (int n1 = 0;n1 < 3;n1++)
	{
		tmp_ang[n1] = -ang[n1];
	}
	
	ogl_camera tmp_cam = cam;
	tmp_cam.focus = 0.0;
	
	base_app * app = base_app::GetAppB();
	for (unsigned int n1 = 0;n1 < app->light_vector.size();n1++)
	{
		if (app->light_vector[n1]->owner != this) continue;
		app->light_vector[n1]->OrbitObject(tmp_ang, tmp_cam);
	}
	
	ogl_dummy_object::RotateObject(tmp_ang, cam);
	
	DoCameraEvents();
}

void ogl_camera::TranslateObject(const GLfloat * dst, const ogl_obj_loc_data * data)
{
	GLfloat tmp_dst[3];
	for (int n1 = 0;n1 < 3;n1++)
	{
		tmp_dst[n1] = -dst[n1];
	}
	
	base_app * app = base_app::GetAppB();
	for (unsigned int n1 = 0;n1 < app->light_vector.size();n1++)
	{
		if (app->light_vector[n1]->owner != this) continue;
		app->light_vector[n1]->TranslateObject(tmp_dst, data);
	}
	
	ogl_dummy_object::TranslateObject(tmp_dst, data);
	
	DoCameraEvents();
}

void ogl_camera::DoCameraEvents(void)
{
	list<ogl_smart_object *>::iterator so_it;
	for (so_it = obj_list.begin();so_it != obj_list.end();so_it++)
	{
cout << "liboglappth : doing a camera_event..." << endl;
		(* so_it)->CameraEvent(* this);
	}
}

/*################################################################################################*/

ogl_transformer_client * ogl_transformer::client = NULL;
bool ogl_transformer::transform_in_progress = false;

ogl_transformer::ogl_transformer() :
	ogl_dummy_object(true)
{
}

ogl_transformer::~ogl_transformer(void)
{
}

void ogl_transformer::Init(ogl_transformer_client * p1)
{
	client = p1;
	
	ogl_obj_loc_data * data = GetLD();
	for (int n1 = 0;n1 < 3;n1++) data->crd[n1] = 0.0;
	data->zdir = oglv3d<GLfloat>(0.0, 0.0, 1.0);
	data->ydir = oglv3d<GLfloat>(0.0, 1.0, 0.0);
}

void ogl_transformer::GetMatrix(GLfloat * p1) const
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix(); glLoadIdentity();
	SetModelView(); glGetFloatv(GL_MODELVIEW_MATRIX, p1);
	glPopMatrix();
}

bool ogl_transformer::BeginTransformation(void)
{
	transform_in_progress = true;
	client->BeginClientTransformation(this);
	return true;
}

bool ogl_transformer::EndTransformation(void)
{
	transform_in_progress = false;
	client->EndClientTransformation(this);
	return true;
}

void ogl_transformer::OrbitObject(const GLfloat * p1, const ogl_camera & p2)
{
	ogl_dummy_object::RotateObject(p1, p2);
}

void ogl_transformer::RotateObject(const GLfloat * p1, const ogl_camera & p2)
{
	ogl_dummy_object::OrbitObject(p1, p2);
}

/*################################################################################################*/

ogl_transformer_client::ogl_transformer_client(void)
{
	tc_object_ref = NULL;
}

ogl_transformer_client::~ogl_transformer_client(void)
{
}

/*################################################################################################*/

// eof
