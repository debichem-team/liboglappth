// OGL_LIGHTS.CPP

// Copyright (C) 1998 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ogl_lights.h"		// config.h is here -> we get ENABLE-macros here...

#include "base_app.h"

#include <vector>
#include <algorithm>	// for WIN32...
#include <sstream>
using namespace std;

// NOT_DEFINED is defined here the same way as in libghemical.

#ifndef NOT_DEFINED
#define NOT_DEFINED -1
#endif	// NOT_DEFINED

/*################################################################################################*/

const GLfloat ogl_light::def_amb_comp[4] = { 0.1, 0.1, 0.1 };
const GLfloat ogl_light::def_diff_comp[4] = { 0.8, 0.8, 0.8 };
const GLfloat ogl_light::def_spec_comp[4] = { 0.8, 0.8, 0.8 };

const ogl_light_components ogl_light::def_components =
{
	(GLfloat *) ogl_light::def_amb_comp,
	(GLfloat *) ogl_light::def_diff_comp,
	(GLfloat *) ogl_light::def_spec_comp
};

ogl_light::ogl_light(const ogl_object_location & p1, const ogl_light_components & p2) :
	ogl_dummy_object(p1)
{
	owner = NULL;
	number = NOT_DEFINED;
	InitComponents(& p2);
}

ogl_light::~ogl_light(void)
{
	delete[] amb_comp;
	delete[] diff_comp;
	delete[] spec_comp;
}

void ogl_light::InitComponents(const ogl_light_components * p1)
{
	amb_comp = new GLfloat[4];
	diff_comp = new GLfloat[4];
	spec_comp = new GLfloat[4];
	
	for (int n1 = 0;n1 < 4;n1++)
	{
		amb_comp[n1] = p1->amb_comp[n1];
		diff_comp[n1] = p1->diff_comp[n1];
		spec_comp[n1] = p1->spec_comp[n1];
	}
}

/*################################################################################################*/

GLfloat ogl_spot_light::size = 0.05;

GLfloat ogl_spot_light::shade1[3] = { 0.0, 0.0, 0.7 };
GLfloat ogl_spot_light::shade2[3] = { 0.0, 0.0, 0.9 };

GLfloat ogl_spot_light::bulb_on[3] = { 1.0, 1.0, 0.5 };
GLfloat ogl_spot_light::bulb_off[3] = { 0.5, 0.5, 0.5 };

const GLfloat ogl_spot_light::def_cutoff = 45.0;
const GLfloat ogl_spot_light::def_exponent = 32.0;

ogl_spot_light::ogl_spot_light(const ogl_object_location & p1, const ogl_light_components & p2, GLfloat p3, GLfloat p4) :
	ogl_light(p1, p2)
{
	GetLD()->crd[3] = 1.0;		// make this a spotlight!!!
	cutoff = p3; exponent = p4;
}

ogl_spot_light::~ogl_spot_light(void)
{
}

void ogl_spot_light::SetupProperties(void)
{
	glLightf((GLenum) number, GL_SPOT_CUTOFF, cutoff);
	glLightf((GLenum) number, GL_SPOT_EXPONENT, exponent);
	
	glLightfv((GLenum) number, GL_AMBIENT, amb_comp);
	glLightfv((GLenum) number, GL_DIFFUSE, diff_comp);
	glLightfv((GLenum) number, GL_SPECULAR, spec_comp);
}

void ogl_spot_light::SetupLocation(void)
{
	glLightfv((GLenum) number, GL_POSITION, GetSafeLD()->crd);
	glLightfv((GLenum) number, GL_SPOT_DIRECTION, GetSafeLD()->zdir.data);
}

/*################################################################################################*/

ogl_directional_light::ogl_directional_light(const ogl_object_location & p1, const ogl_light_components & p2) :
	ogl_light(p1, p2)
{
}

ogl_directional_light::~ogl_directional_light(void)
{
}

void ogl_directional_light::SetupProperties(void)
{
	glLightf((GLenum) number, GL_SPOT_CUTOFF, 180.0);
	glLightf((GLenum) number, GL_SPOT_EXPONENT, 0.0);
	
	glLightfv((GLenum) number, GL_AMBIENT, amb_comp);
	glLightfv((GLenum) number, GL_DIFFUSE, diff_comp);
	glLightfv((GLenum) number, GL_SPECULAR, spec_comp);
}

// the direction of a directional light is the reversed z-axis direction!!!
// the direction of a directional light is the reversed z-axis direction!!!
// the direction of a directional light is the reversed z-axis direction!!!

void ogl_directional_light::SetupLocation(void)
{
	GLfloat tmp_crd[4]; tmp_crd[3] = 0.0;	// make this a directional light!!!
	
	for (int n1 = 0;n1 < 3;n1++) tmp_crd[n1] = -GetSafeLD()->zdir[n1];
	glLightfv((GLenum) number, GL_POSITION, tmp_crd);
}

/*################################################################################################*/

// eof
