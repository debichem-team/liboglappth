// OGL_OBJECTS.CPP

// Copyright (C) 1998 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "ogl_objects.h"	// config.h is here -> we get ENABLE-macros here...

#ifdef WIN32
#include <windows.h>	// need to have this before GL stuff...
#endif	// WIN32

#include <GL/gl.h>

#include <algorithm>
using namespace std;

#include "ogl_camera.h"

// NOT_DEFINED is defined here the same way as in libghemical.

#ifndef NOT_DEFINED
#define NOT_DEFINED -1
#endif	// NOT_DEFINED

/*################################################################################################*/

void TransformVector(oglv3d<GLfloat> & p1, const GLfloat * p2)
{
	int n1; int n2; GLfloat pv[4]; pv[3] = 1.0;
	for (n1 = 0;n1 < 3;n1++) pv[n1] = p1.data[n1];
	
	GLfloat rv[4] = { 0.0, 0.0, 0.0, 0.0 };
	for (n1 = 0;n1 < 4;n1++) for (n2 = 0;n2 < 4;n2++) rv[n1] += pv[n2] * p2[n2 * 4 + n1];
	for (n1 = 0;n1 < 3;n1++) p1.data[n1] = rv[n1] / rv[3];
}

void SetModelView(const ogl_obj_loc_data * data)
{
	glTranslatef(data->crd[0], data->crd[1], data->crd[2]);
	
	oglv3d<GLfloat> gzd(0.0, 0.0, 1.0);
	const oglv3d<GLfloat> gyd(0.0, 1.0, 0.0);
	
	// first the global y-direction is rotated so that it is parallel to the local
	// y-direction. this rotation is done using the cross product vector of the global
	// and local y-directions, or if it has zero length, using the global x-direction.
	
	oglv3d<GLfloat> tmpv1 = gyd.vpr(data->ydir); GLfloat len = tmpv1.len();
	if (len == 0.0) tmpv1 = oglv3d<GLfloat>(1.0, 0.0, 0.0);
	else tmpv1 = tmpv1 / len;
	
	GLfloat ang1 = 180.0 * gyd.ang(data->ydir) / M_PI;
	
	glPushMatrix(); glLoadIdentity();
	glRotatef(ang1, tmpv1[0], tmpv1[1], tmpv1[2]);
	GLfloat rotm[16]; glGetFloatv(GL_MODELVIEW_MATRIX, rotm);
	glPopMatrix();
	
	glMultMatrixf(rotm);
	TransformVector(gzd, rotm);
	
	// after that the global z-direction is rotated so that it is parallel to the local
	// z-direction. this rotation is done using the common y-direction. direction of the
	// rotation must be checked because the range is now a full circle, 360.0 degrees...
	
	oglv3d<GLfloat> xdir = data->ydir.vpr(data->zdir);
	GLfloat ang2 = 180.0 * gzd.ang(data->zdir) / M_PI;
	if (gzd.spr(xdir) > 0.0) ang2 = 360.0 - ang2;
	glRotatef(ang2, 0.0, 1.0, 0.0);
}

/*################################################################################################*/

ogl_object_location::ogl_object_location(void)
{
	data = new ogl_obj_loc_data;
	data->lock_count = 1;
}

ogl_object_location::ogl_object_location(const ogl_object_location & p1)
{
	data = p1.data;
	data->lock_count++;
}

ogl_object_location::~ogl_object_location(void)
{
	data->lock_count--;
	if (!data->lock_count) delete data;
}

void ogl_object_location::SetModelView(void) const
{
	UpdateLocation();		// update the location if it's dynamic!!!
	::SetModelView(data);
}

const ogl_obj_loc_data * ogl_object_location::GetSafeLD(void) const
{
	return data;
}

/*################################################################################################*/

ogl_ol_static::ogl_ol_static(void) :
	ogl_object_location()
{
	for (int n1 = 0;n1 < 3;n1++) data->crd[n1] = 0.0;
	data->zdir = oglv3d<GLfloat>(0.0, 0.0, 1.0);
	data->ydir = oglv3d<GLfloat>(0.0, 1.0, 0.0);
}

ogl_ol_static::ogl_ol_static(const ogl_obj_loc_data * p1) :
	ogl_object_location()
{
	for (int n1 = 0;n1 < 3;n1++) data->crd[n1] = p1->crd[n1];
	data->zdir = p1->zdir;
	data->ydir = p1->ydir;
}

ogl_ol_static::~ogl_ol_static(void)
{
}

void ogl_ol_static::UpdateLocation(void) const
{
}

ogl_object_location * ogl_ol_static::MakeCopy(void) const
{
	return new ogl_ol_static(* this);
}

/*################################################################################################*/

ogl_ol_dynamic_l1::ogl_ol_dynamic_l1(void) :
	ogl_object_location()
{
	UpdateLocation();
}

ogl_ol_dynamic_l1::~ogl_ol_dynamic_l1(void)
{
}

// just move the object somewhere... a dummy function for testing purposes...
// just move the object somewhere... a dummy function for testing purposes...
// just move the object somewhere... a dummy function for testing purposes...

void ogl_ol_dynamic_l1::UpdateLocation(void) const
{
	data->crd[0] += 0.01;
}

ogl_object_location * ogl_ol_dynamic_l1::MakeCopy(void) const
{
	return new ogl_ol_dynamic_l1(* this);
}

/*################################################################################################*/

ogl_dummy_object::ogl_dummy_object(bool p1)
{
	if (!p1) ol = NULL;
	else ol = new ogl_ol_static();
	
	my_id_number = NOT_DEFINED;
}

ogl_dummy_object::ogl_dummy_object(const ogl_object_location & p1)
{
	ol = p1.MakeCopy();
	
	my_id_number = NOT_DEFINED;
}

ogl_dummy_object::~ogl_dummy_object(void)
{
	if (ol != NULL) delete ol;
}

void ogl_dummy_object::SetModelView(void) const
{
	if (ol != NULL) ol->SetModelView();
}

const ogl_obj_loc_data * ogl_dummy_object::GetSafeLD(void) const
{
	return (ol != NULL ? ol->data : NULL);
}

ogl_obj_loc_data * ogl_dummy_object::GetLD(void) const
{
	return (ol != NULL ? ol->data : NULL);
}

// this will orbit the object around the camera's focus point. the rotation is done relatively
// to the camera's direction vectors. basic idea is to make first a 4x4-transformation matrix and
// use it to manipulate the object's direction and position vectors.

void ogl_dummy_object::OrbitObject(const GLfloat * ang, const ogl_camera & cam)
{
	if (ol == NULL) return;
	
	const ogl_obj_loc_data * loc1 = cam.GetSafeLD();
	oglv3d<GLfloat> cam_xdir = loc1->ydir.vpr(loc1->zdir);
	
	oglv3d<GLfloat> tmpv1 = oglv3d<GLfloat>((GLfloat *) loc1->crd, GetSafeLD()->crd);
	oglv3d<GLfloat> tmpv2 = loc1->zdir * cam.focus;
	oglv3d<GLfloat> tmpv3 = tmpv1 - tmpv2;
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix(); glLoadIdentity();
	glRotatef(ang[0], cam_xdir[0], cam_xdir[1], cam_xdir[2]);
	glRotatef(ang[1], loc1->ydir[0], loc1->ydir[1], loc1->ydir[2]);
	glRotatef(ang[2], loc1->zdir[0], loc1->zdir[1], loc1->zdir[2]);
	GLfloat rotm[16]; glGetFloatv(GL_MODELVIEW_MATRIX, rotm); glPopMatrix();
	
	ogl_obj_loc_data * loc2 = GetLD();
	oglv3d<GLfloat> xdir = loc2->ydir.vpr(loc2->zdir);
	
	TransformVector(xdir, rotm);
	TransformVector(loc2->ydir, rotm);
	
	loc2->zdir = xdir.vpr(loc2->ydir);
	loc2->zdir = loc2->zdir / loc2->zdir.len();
	loc2->ydir = loc2->ydir / loc2->ydir.len();
	
	TransformVector(tmpv3, rotm);
	for (int n1 = 0;n1 < 3;n1++)
	{
		loc2->crd[n1] = loc1->crd[n1] + tmpv2[n1] + tmpv3[n1];
	}
}

// this will rotate the object relatively to the camera's direction vectors. the basic idea
// is similar to that in dummy_object::OrbitObject().

void ogl_dummy_object::RotateObject(const GLfloat * ang, const ogl_camera & cam)
{
	if (ol == NULL) return;
	
	const ogl_obj_loc_data * loc1 = cam.GetSafeLD();
	oglv3d<GLfloat> cam_xdir = loc1->ydir.vpr(loc1->zdir);
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix(); glLoadIdentity();
	glRotatef(ang[0], cam_xdir[0], cam_xdir[1], cam_xdir[2]);
	glRotatef(ang[1], loc1->ydir[0], loc1->ydir[1], loc1->ydir[2]);
	glRotatef(ang[2], loc1->zdir[0], loc1->zdir[1], loc1->zdir[2]);
	GLfloat rotm[16]; glGetFloatv(GL_MODELVIEW_MATRIX, rotm); glPopMatrix();
	
	ogl_obj_loc_data * loc2 = GetLD();
	oglv3d<GLfloat> xdir = loc2->ydir.vpr(loc2->zdir);
	
	TransformVector(xdir, rotm);
	TransformVector(loc2->ydir, rotm);
	
	loc2->zdir = xdir.vpr(loc2->ydir);
	loc2->zdir = loc2->zdir / loc2->zdir.len();
	loc2->ydir = loc2->ydir / loc2->ydir.len();
}

// this will translate the object relatively to given direction vectors. this is just a simple
// summing, no more strange matrix tricks here...

void ogl_dummy_object::TranslateObject(const GLfloat * dst, const ogl_obj_loc_data * data)
{
	if (ol == NULL || data == NULL) return;
	
	oglv3d<GLfloat> obj_xdir = data->ydir.vpr(data->zdir);
	ogl_obj_loc_data * loc1 = GetLD();
	
	for (int n1 = 0;n1 < 3;n1++)
	{
		GLfloat tmp1 = dst[0] * obj_xdir[n1];
		GLfloat tmp2 = dst[1] * data->ydir[n1];
		GLfloat tmp3 = dst[2] * data->zdir[n1];
		loc1->crd[n1] += tmp1 + tmp2 + tmp3;
	}
}

/*################################################################################################*/

ogl_smart_object::ogl_smart_object(void) :
	ogl_dummy_object(false)
{
	transparent = false;
}

ogl_smart_object::ogl_smart_object(const ogl_object_location & p1) :
	ogl_dummy_object(p1)
{
	transparent = false;
}

ogl_smart_object::~ogl_smart_object(void)
{
	list<ogl_camera *>::iterator c_it;
	for (c_it = cam_list.begin();c_it != cam_list.end();c_it++)
	{
		ogl_camera * cam = (* c_it);
		
		bool flag = true;
		while (flag)
		{
			list<ogl_smart_object *>::iterator so_it;
			so_it = find(cam->obj_list.begin(), cam->obj_list.end(), this);
			if (so_it != cam->obj_list.end()) cam->obj_list.erase(so_it);
			else flag = false;
		}
	}
}

void ogl_smart_object::ConnectCamera(ogl_camera & p1)
{
	ogl_camera * cam = & p1;
	
	cam_list.push_back(cam);
	cam->obj_list.push_back(this);
}

/*################################################################################################*/

// eof
