// TRANSPARENT.CPP

// Copyright (C) 1999 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

#include "transparent.h"

/*################################################################################################*/

transparent_primitive::transparent_primitive(void)
{
	owner = NULL; data = NULL;
	z_distance = 0.0;
}

transparent_primitive::transparent_primitive(void * p1, transparent_primitive_data & p2)
{
	owner = p1; data = & p2;
	z_distance = 0.0;
}

transparent_primitive::transparent_primitive(const transparent_primitive & p1)
{
	owner = p1.owner; data = p1.data;
	z_distance = p1.z_distance;
}

transparent_primitive::~transparent_primitive(void)
{
}

bool transparent_primitive::TestOwner(void * p1) const
{
	return (owner == p1);
}

transparent_primitive_data * transparent_primitive::GetData(void) const
{
	return data;
}

void transparent_primitive::UpdateDistance(const GLfloat * crd_c, const GLfloat * zdir)
{
	GLfloat crd_p[3];
	crd_p[0] = data->midpoint[0] - crd_c[0];
	crd_p[1] = data->midpoint[1] - crd_c[1];
	crd_p[2] = data->midpoint[2] - crd_c[2];
	
// if we mark the vector from crd_c to crd_p as v1, and angle between v1 and zdir as angle
// alpha, we have
//
//	zdist = cos(alpha) * |v1|			, where |v1| = length of v1. since
//
//	cos(alpha) = ip(v1,zdir) / (|zdir| * |v1|)	, we have
//
//	zdist = ip(v1,zdir) / |zdir|			, and if zdir is a unit vector,
//
//	zdist = ip(v1,zdir)
	
	z_distance = crd_p[0] * zdir[0] + crd_p[1] * zdir[1] + crd_p[2] * zdir[2];
}

bool transparent_primitive::operator<(const transparent_primitive & p1) const
{
	return (z_distance > p1.z_distance);	// inverted order...
}

/*################################################################################################*/

transparent_primitive_data::transparent_primitive_data(void)
{
}

transparent_primitive_data::~transparent_primitive_data(void)
{
}

/*################################################################################################*/

tpd_tri_3c::tpd_tri_3c(GLfloat * c1, GLfloat * p1, GLfloat * c2, GLfloat * p2, GLfloat * c3, GLfloat * p3)
{
	color[0] = c1;
	color[1] = c2;
	color[2] = c3;
	
	point[0] = p1;
	point[1] = p2;
	point[2] = p3;
	
	UpdateMP();
}

tpd_tri_3c::~tpd_tri_3c(void)
{
}

void tpd_tri_3c::Render(void)
{
	glBegin(GL_TRIANGLES);
	
	glColor4fv(color[0]);
	glVertex3fv(point[0]);
	
	glColor4fv(color[1]);
	glVertex3fv(point[1]);
	
	glColor4fv(color[2]);
	glVertex3fv(point[2]);
	
	glEnd();	// GL_TRIANGLES
}

void tpd_tri_3c::UpdateMP(void)
{
	midpoint[0] = (point[0][0] + point[1][0] + point[2][0]) / 3.0;
	midpoint[1] = (point[0][1] + point[1][1] + point[2][1]) / 3.0;
	midpoint[2] = (point[0][2] + point[1][2] + point[2][2]) / 3.0;
}

/*################################################################################################*/

tpd_quad_4c::tpd_quad_4c(GLfloat * c1, GLfloat * p1, GLfloat * c2, GLfloat * p2, GLfloat * c3, GLfloat * p3, GLfloat * c4, GLfloat * p4)
{
	color[0] = c1;
	color[1] = c2;
	color[2] = c3;
	color[3] = c4;
	
	point[0] = p1;
	point[1] = p2;
	point[2] = p3;
	point[3] = p4;
	
	UpdateMP();
}

tpd_quad_4c::~tpd_quad_4c(void)
{
}

void tpd_quad_4c::Render(void)
{
	glBegin(GL_QUADS);
	
	glColor4fv(color[0]);
	glVertex3fv(point[0]);
	
	glColor4fv(color[1]);
	glVertex3fv(point[1]);
	
	glColor4fv(color[2]);
	glVertex3fv(point[2]);
	
	glColor4fv(color[3]);
	glVertex3fv(point[3]);
	
	glEnd();	// GL_QUADS
}

void tpd_quad_4c::UpdateMP(void)
{
	midpoint[0] = (point[0][0] + point[1][0] + point[2][0] + point[3][0]) / 4.0;
	midpoint[1] = (point[0][1] + point[1][1] + point[2][1] + point[3][1]) / 4.0;
	midpoint[2] = (point[0][2] + point[1][2] + point[2][2] + point[3][2]) / 4.0;
}

/*################################################################################################*/

// eof
