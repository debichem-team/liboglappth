// TRANSPARENT.H : utility classes for transparent primitive rendering.

// Copyright (C) 1999 Tommi Hassinen.

// This package is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this package; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

/*################################################################################################*/

//#include "config.h"

#ifndef TRANSPARENT_H
#define TRANSPARENT_H

/*################################################################################################*/

#ifdef WIN32
#include <windows.h>	// need to have this before the GL stuff...
#endif	// WIN32

#include <GL/gl.h>
#include <GL/glu.h>

#include <stdlib.h>	// the definition for NULL...

/*################################################################################################*/

class transparent_primitive;
class transparent_primitive_data;

class tpd_tri_3c;
class tpd_quad_4c;

/*################################################################################################*/

/// An utility class for transparency effects.

class transparent_primitive
{
	private:
	
	GLfloat z_distance; void * owner;
	transparent_primitive_data * data;
	
	public:
	
	transparent_primitive(void);
	transparent_primitive(void *, transparent_primitive_data &);
	transparent_primitive(const transparent_primitive &);
	~transparent_primitive(void);
	
	bool TestOwner(void *) const;
	transparent_primitive_data * GetData(void) const;
	
	void UpdateDistance(const GLfloat *, const GLfloat *);
	
	bool operator<(const transparent_primitive &) const;
};

class transparent_primitive_data
{
	protected:
	
	GLfloat midpoint[3];
	
	public:
	
	transparent_primitive_data(void);
	virtual ~transparent_primitive_data(void);
	
	virtual void Render(void) = 0;
	virtual void UpdateMP(void) = 0;
	
	friend class transparent_primitive;
};

/// A triangle with 3 colors.

class tpd_tri_3c : public transparent_primitive_data
{
	protected:
	
	GLfloat * color[3];
	GLfloat * point[3];
	
	public:
	
	tpd_tri_3c(GLfloat *, GLfloat *, GLfloat *, GLfloat *, GLfloat *, GLfloat *);
	~tpd_tri_3c(void);
	
	void Render(void);		// virtual
	void UpdateMP(void);		// virtual
};

/// A quadrilateral with 4 colors.

class tpd_quad_4c : public transparent_primitive_data
{
	protected:
	
	GLfloat * color[4];
	GLfloat * point[4];
	
	public:
	
	tpd_quad_4c(GLfloat *, GLfloat *, GLfloat *, GLfloat *, GLfloat *, GLfloat *, GLfloat *, GLfloat *);
	~tpd_quad_4c(void);
	
	void Render(void);		// virtual
	void UpdateMP(void);		// virtual
};

/*################################################################################################*/

#endif	// TRANSPARENT_H

// eof
